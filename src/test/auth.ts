import request from "supertest";
import { app } from "../app";

export const testUser = {
  name: "testUser",
  email: "testuser@gmail.com",
  password: "123456789",
  age: 22,
  role: "user",
};

export const testAdmin = {
  name: "testAdmin",
  email: "testadmin@gmail.com",
  password: "123456789",
  age: 22,
  role: "admin",
};

let token = null;

export const authUser = async (): Promise<string> => {
  await request(app)
    .post("/api/users/loginUser")
    .send({
      email: testUser.email,
      password: testUser.password,
    })
    .then((res) => {
      token = res.body.token;
      //return token;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
  return token;
};

export const authAdmin = async (): Promise<string> => {
  await request(app)
    .post("/api/users/loginAdmin")
    .send({
      email: testAdmin.email,
      password: testAdmin.password,
    })
    .then((res) => {
      token = res.body.token;
      console.log("Admin token : " + token);
      //return token;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
  return token;
};
