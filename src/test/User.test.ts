import request from "supertest";
import { app } from "../app";
import User from "../models/user";
import { testUser, testAdmin, authUser } from "./auth";

beforeEach(async () => {
  await User.deleteMany({});
  await new User(testUser).save();
  await new User(testAdmin).save();
});

test("Should be able to register as a user", async () => {
  const id = await request(app)
  .post("/api/users/registerUser")
  .send({
    name: "User2",
    email: "user2@gmail.com",
    password: "123456789",
    age: 22,
  })
  .expect(201)
  .then((res)=>{
     return res.body.user._id;
  })
  const user = await User.findOne({_id:id});
  expect(user.email).toBeTruthy();
})

test("Should not able to register with same email twice", async () => {
  const id = await request(app)
  .post("/api/users/registerUser")
  .send({
    name: "User2",
    email: testUser.email,
    password: "123456789",
    age: 22,
  })
  .expect(500)
  .then((res) => {
    return res.body._id;
  })
  const user = await User.findOne({_id:id});
  expect(user).toBe(null);
})

test("Should not able to register without all required data", async () => {
  await request(app)
  .post("/api/users/registerUser")
  .send({
    email: "user3@gmail.com",
    password: "123456789",
    age: 22,
  })
  .expect(500)
  const user = await User.findOne({email:"user3@gmail.com"});
  expect(user).toBe(null);
})

test("Should be able to register as admin", async () => {
  const id = await request(app)
    .post("/api/users/registerAdmin")
    .send({
      name: "admin",
      email: "admin@gmail.com",
      password: "123456789",
      age: 22,
    }).expect(201)
    .then((res)=>{
       return res.body.user._id;
    })
    const admin = await User.findOne({_id:id});
    expect(admin.email).toBeTruthy();
    expect(admin.role).toBe('admin');
});

test("Should able to login as user", async () => {
  const id = await request(app)
    .post("/api/users/loginUser")
    .send({
      email: testUser.email,
      password: testUser.password,
    })
    .expect(201)
    .then((res) => {
      return res.body.user._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    const user = await User.findOne({_id: id});
    expect(user.role).toBe("user");
    expect(user.email).toBe(testUser.email);
});

test("Should not able to login without password", async () => {
  const id = await request(app)
    .post("/api/users/loginUser")
    .send({
      email: testUser.email,
    })
    .expect(500)
    .then((res) => {
      return res.body.user._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    const user = await User.findOne({_id: id});
    expect(user).toBe(null);
});

test("Should not able to login without valid email", async () => {
  const id = await request(app)
    .post("/api/users/loginUser")
    .send({
      email: "rishabh",
    })
    .expect(401)
    .then((res) => {
      return res.body.user._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    const user = await User.findOne({_id: id});
    expect(user).toBe(null);
});

test("Should not able to login with wrong password", async () => {
  const id = await request(app)
    .post("/api/users/loginUser")
    .send({
      email: testUser.email,
      password: "wrongpassword",
    })
    .expect(401)
    .then((res) => {
      return res.body.user._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    const user = await User.findOne({_id: id});
    expect(user).toBe(null);
});

test("Should able to login as admin", async () => {
  const id = await request(app)
    .post("/api/users/loginAdmin")
    .send({
      email: testAdmin.email,
      password: testAdmin.password,
    })
    .expect(201)
    .then((res) => {
      return res.body.user._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });

    const admin = await User.findOne({_id:id});
    expect(admin.role).toBe("admin");
    expect(admin.email).toBe(testAdmin.email);
});

test("Should able to get the user profile", async () => {
  const token = await authUser();
  const user = await request(app)
    .get("/api/users/me")
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res) => {
      return res.body.user;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    expect(user).not.toStrictEqual({});
});

test("Should not able to get the user profile without login", async () => {
  const user = await request(app)
    .get("/api/users/me")
    .expect(401)
    .then((res) => {
      return res.body.user;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    expect(user).toBe(undefined);
});