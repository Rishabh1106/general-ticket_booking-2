import request from "supertest";
import { app } from "../app";
import { authUser, authAdmin } from "./auth";
import Place from "../models/place";
import Ticket from "../models/ticket";

// make test place
// in beofeEach clear db and create this new one
// start writing the test for apis

const testPlace = {
  _id: "61d3bf5b67e1bbd5b617d6cf",
  name: "The Royal Hotel",
  place: "Hotel",
  location: "Subhash Nagar 2nd,Kota",
  peopleInvolved: ["A", "B"],
  duration: "2hr",
  price: 500,
  totalSpots: 50,
  startTime: "2021-11-29T06:30:00.530",
  endTime: "2021-11-29T06:30:00.530",
  type: "AC",
  spots: [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24, 25, 26, 27, 28, 29, 30,
  ],
};

beforeEach(async () => {
  await Place.deleteMany({});
  await new Place(testPlace).save();
});

const getIdofTicket = async () => {
  const token = await authUser();
  const id = await request(app)
    .get("/api/place/getMyTickets")
    .set("Authorization", `Bearer ${token}`)
    .then((res) => {
      return res.body[0]._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    console.log(id);
  return id;
};

test("Should add a place", async () => {
  const token = await authAdmin();
  const id = await request(app)
    .post("/api/place/addPlace")
    .send({
      name: "The Royal Hotel 2",
      place: "Hotel",
      location: "Subhash Nagar 2nd,Kota",
      peopleInvolved: ["A", "B"],
      duration: "2hr",
      price: 500,
      totalSpots: 50,
      startTime: "2021-11-29T06:30:00.530",
      endTime: "2021-11-29T06:30:00.530",
      type: "AC",
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res)=>{
      return res.body._id;
  })
    const place = await Place.findOne({_id:id}); 
    expect(place.name).toBeTruthy();
});

test("Should not allow to add a place without name", async () => {
  const token = await authAdmin();
  const id = await request(app)
    .post("/api/place/addPlace")
    .send({
      place: "Hotel",
      location: "Subhash Nagar 2nd,Kota",
      peopleInvolved: ["A", "B"],
      duration: "2hr",
      price: 500,
      totalSpots: 50,
      startTime: "2021-11-29T06:30:00.530",
      endTime: "2021-11-29T06:30:00.530",
      type: "AC",
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body._id;
    })
    const place = await Place.findOne({_id:id});
    expect(place).toBe(null);
});

test("Should get all the places", async () => {
  const token = await authUser();
  const places = await request(app)
    .get("/api/place/getAllPlaces")
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res)=>{
      return res.body;
    })
    expect(places.length>0).toBeTruthy();
});

test("Should not able to get all the places if user is not logged in", async () => {
    const places = await request(app)
    .get("/api/place/getAllPlaces")
    .expect(401)
    .then((res) => {
      return res.body.user;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
    expect(places).toBe(undefined);
});

test("Should get the specific place", async () => {
  const token = await authUser();
  const id = await request(app)
    .get(`/api/place/getPlace/${testPlace._id}`)
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res)=>{
      return res.body._id;
    })
    const place = await Place.findOne({_id:id})
    expect(place.name).toBe(testPlace.name);
});

test("Should not get the place with wrong id", async () => {
  const token = await authUser();
  const place = await request(app)
    .get(`/api/place/getPlace/wrongId}`)
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body;
    })
    expect(place).toStrictEqual({});
});

test("Should be able to book a ticket", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/place/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      spotNumber: 10,
      category: "Hotel",
      categoryId: testPlace._id,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket._id).toBeTruthy();
});

test("Should not be able to book a ticket with wrong categoryId", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/place/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      spotNumber: 10,
      category: "Hotel",
      categoryId: "wrongId",
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket).toBe(null);
});

test("Should not be able to book a ticket with incorrect info", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/place/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      categoryId: "wrongId",
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket).toBe(null);
});

test("Should able to cancel a ticket", async () => {
  const token = await authUser();
  const id = await getIdofTicket();
  const resTicketId = await request(app)
    .patch(`/api/place/cancelTicket/${id}`)
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res) => {
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:resTicketId});
    expect(ticket.status).toBe("Cancelled");
});

test("Should not be able to cancel a ticket with unknown id", async () => {
  const token = await authUser();
  const id = await getIdofTicket();
  const resTicketId = await request(app)
    .patch(`/api/place/cancelTicket/unknownId`)
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res) => {
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:resTicketId});
    expect(ticket).toBe(null);
});