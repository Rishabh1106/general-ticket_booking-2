import request from "supertest";
import { app } from "../app";
import { authUser, authAdmin } from "./auth";
import Vehicle from "../models/vehicle";
import Ticket from "../models/ticket";

const testVehicle = {
  _id: "61d3bf5b67e1bbd5b617d6cf",
  vehicleNumber: "RJ-SA-TEST",
  vehicleName: "Test Bus Service",
  vehicle: "Bus",
  source: "S1",
  destination: "D1",
  totalSeats: 30,
  startDate: "2021-11-29T06:30:00.530",
  endDate: "2021-11-30T06:30:00.530",
  fare: 200,
  seats: [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24, 25, 26, 27, 28, 29, 30,
  ],
};

beforeEach(async () => {
  await Vehicle.deleteMany({});
  await new Vehicle(testVehicle).save();
});

const getIdofTicket = async () => {
  const token = await authUser();
  const id = await request(app)
    .get("/api/travel/getMyTickets")
    .set("Authorization", `Bearer ${token}`)
    .then((res) => {
      return res.body[0]._id;
    })
    .catch((err) => {
      console.log("this is err " + err);
    });
  return id;
};

test("Should add a vehicle", async () => {
  const token = await authAdmin();
  const id = await request(app)
    .post("/api/travel/addVehicle")
    .send({
      vehicleNumber: "RJ-SA-2145",
      vehicleName: "Excellent Bus Service",
      vehicle: "Bus",
      source: "C",
      destination: "D",
      totalSeats: 30,
      startDate: "2021-11-29T06:30:00.530",
      endDate: "2021-11-30T06:30:00.530",
      fare: 200,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res)=>{
      return res.body._id;
  })
  const vehicle = await Vehicle.findOne({_id:id}); 
  expect(vehicle.vehicleNumber).toBe('RJ-SA-2145');
});

test("Should not allow to add a vehicle without required data", async () => {
  const token = await authAdmin();
  const id = await request(app)
    .post("/api/travel/addVehicle")
    .send({
      vehicleNumber: "RJ-SA-2145",
      vehicle: "Bus",
      source: "C",
      destination: "D",
      totalSeats: 30,
      startDate: "2021-11-29T06:30:00.530",
      endDate: "2021-11-30T06:30:00.530",
      fare: 200,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body._id;
  })
  const vehicle = await Vehicle.findOne({_id:id}); 
  expect(vehicle).toBe(null);
});

test("Should not allow to add a vehicle as User role", async () => {
  const token = await authUser();
  const id = await request(app)
    .post("/api/travel/addVehicle")
    .send({
      vehicleNumber: "RJ-SA-2145",
      vehicleName: "Excellent Bus Service",
      vehicle: "Bus",
      source: "C",
      destination: "D",
      totalSeats: 30,
      startDate: "2021-11-29T06:30:00.530",
      endDate: "2021-11-30T06:30:00.530",
      fare: 200,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(403)
    const vehicle = await Vehicle.findOne({vehicleNumber: "RJ-SA-2145"}); 
    expect(vehicle).toBe(null);
});

test("Should reset a vehicle", async () => {
  const token = await authAdmin();
  const id = await request(app)
    .post(`/api/travel/resetVehicle/${testVehicle._id}`)
    .send()
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res)=>{
      return res.body._id;
    })
    const vehicle = await Vehicle.findOne({_id:id})
    expect(vehicle.seats.length).toBe(testVehicle.totalSeats);
});

test("Should not be able to reset a vehicle as a user role", async () => {
  const token = await authUser();
  const id = await request(app)
    .post(`/api/travel/resetVehicle/${testVehicle._id}`)
    .send()
    .set("Authorization", `Bearer ${token}`)
    .expect(403)
    .then((res)=>{
      return res.body._id;
    })
    const vehicle = await Vehicle.findOne({_id:id})
    expect(vehicle).toBe(null);
});

test("Should able to book a ticket", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/travel/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      spotNumber: 10,
      category: "Bus",
      categoryId: testVehicle._id,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket._id).toBeTruthy();
});

test("Should not be able to book a ticket with invalid categoryId", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/travel/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      spotNumber: 10,
      category: "Bus",
      categoryId: "invalid id",
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket).toBe(null);
});

test("Should not be able to book a ticket with incomplete info", async () => {
  const token = await authUser();
  const ticketId = await request(app)
    .post("/api/travel/bookTicket")
    .send({
      name: "Rishabh",
      age: 22,
      category: "Bus",
      categoryId: testVehicle._id,
    })
    .set("Authorization", `Bearer ${token}`)
    .expect(403)
    .then((res)=>{
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:ticketId})
    expect(ticket).toBe(null);
});

test("Should able to cancel a ticket", async () => {
  const token = await authUser();
  const id = await getIdofTicket();
  console.log("Ticket id : ", id);
  const resTicketId = await request(app)
    .patch(`/api/travel/cancelTicket/${id}`)
    .set("Authorization", `Bearer ${token}`)
    .expect(201)
    .then((res) => {
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:resTicketId});
    expect(ticket.status).toBe("Cancelled");
});

test("Should not able to cancel a ticket with unknown id", async () => {
  const token = await authUser();
  const id = await getIdofTicket();
  console.log("Ticket id : ", id);
  const resTicketId = await request(app)
    .patch(`/api/travel/cancelTicket/testId`)
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .then((res) => {
      return res.body._id;
    })
    const ticket = await Ticket.findOne({_id:resTicketId});
    expect(ticket).toBe(null);
});

test("Should get all the tickets", async () => {
  const token = await authAdmin();
  const tickets = await request(app)
    .get("/api/travel/getAllTickets")
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res)=>{
      return res.body;
    })
    console.log(tickets);
    expect(tickets.length>0).toBeTruthy();
});

test("Should get all the vehicles", async () => {
  const token = await authUser();
  const vehicles = await request(app)
    .get("/api/travel/getAllVehicles")
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res)=>{
      return res.body;
    })
    console.log(vehicles);
    expect(vehicles.length>0).toBeTruthy();
});

test("Should get the specific vehicle", async () => {
  const token = await authUser();
  const id = await request(app)
    .get(`/api/travel/getVehicle/${testVehicle._id}`)
    .set("Authorization", `Bearer ${token}`)
    .expect(200)
    .then((res)=>{
      return res.body._id;
    })
    const vehicle = await Vehicle.findOne({_id:id})
    expect(vehicle.vehicleNumber).toBe(testVehicle.vehicleNumber);
});