import { Request, Response } from "express";
import { handleError,} from "./customErrorClass";

export const errorResponder = (
  err,
  req: Request,
  res: Response
) => {
  if(err.code === 11000){
    const code = err.errmsg.match(/(["'])(?:(?=(\\?))\2.)*?\1/)[0];
    const msg = `Duplicate field value: ${code}. Please use anothe value!`;
    res.status(400).send(msg)
    }
  if(err.name === 'ValidationError'){
    const msg = `Invalid input data.`;
    res.status(400).send(msg)
  }
  handleError(err,res);
};
