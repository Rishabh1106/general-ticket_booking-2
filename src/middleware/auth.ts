import User, { UserRequest } from "../models/user";
import { Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import Logger from "../loggers/dev-logger";


export const auth = async (
  req: UserRequest,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    const token = req.header("Authorization").replace("Bearer ", "");

    if (!token) {
      return res.status(401).json({
        status: "fail",
        message: "You are not logged in! Please login to get access",
      });
    }

    const decode = jwt.verify(token, process.env.jwt_secret_key);
    const user = await User.findOne({ _id: decode.id });
    if (!user) {
      return res.status(401).json({
        status: "fail",
        message: "User no longer exists",
      });
    }
    req.user = user;
    next();
  } catch (e) {
    Logger.error(`Error Status Code : 401 - Message :  "User is not logged in!" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
    return res.status(401).send({ error: "Please Login!" });
  }
};

export const adminAccess = async (
  req: UserRequest,
  res: Response,
  next: NextFunction
): Promise<void | Response> => {
  if (req.user.role != "admin") {
    Logger.error(`Error Status Code : 403 - Message :  "You don't have access" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
    return res.status(403).send({ error: "You don't have access!" });
  }
  next();
};