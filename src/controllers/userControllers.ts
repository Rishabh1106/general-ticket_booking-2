import { UserRequest } from "../models/user";
import { Request, Response } from "express";
import { ValidationError } from "../middleware/customErrorClass";

import {
  createAdmin,
  createUser,
  getTickets,
  signToken,
  validateAdminUser,
  validateUser,
} from "../services/user.service";
import Logger  from "../loggers/dev-logger";

export const registerAdmin = async (
  req: Request, 
  res: Response
  ): Promise<Response> => {
    const user = await createAdmin(req.body);
    return res.status(201).send({ user: user });
  }

export const loginAdmin = async (
  req: Request, 
  res: Response
  ): Promise<Response> => {
    const user = await validateAdminUser(req.body.email, req.body.password);
    if (!user) {
      Logger.error(`Error Status Code : 401 - Message :  "Invalid Email or Password" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(401).send("Invalid email or password");
    }
    const token = signToken(user._id);
    return res.status(201).send({ user: user, token: token });
  }


export const registerUser = async (
  req: Request, res: Response
  ): Promise<Response> => {
    const user = await createUser(req.body);
    if (!user) {
      throw new ValidationError(
        "Could not create user, check email passowrd again.",
        401
      );
    }
    return res.status(201).send({ user: user });
  }

export const loginUser = async (
  req: Request, 
  res: Response
  ): Promise<Response> => {
    const user = await validateUser(req.body.email, req.body.password);
    if (!user) {
      Logger.error(`Error Status Code : 401 - Message :  "Invalid Email or Password" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(401).send("Invalid email or password")
    }
    const token = signToken(user._id);
    return res.status(201).send({ user: user, token: token });
  }

export const usersMe = async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const tickets = await getTickets(req.user._id);
    return res.send({
      user: req.user,
      tickets: tickets,
    });
  }