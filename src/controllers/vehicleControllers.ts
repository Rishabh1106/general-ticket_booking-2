import { Response } from "express";
import { UserRequest } from "../models/user";
import {
  addVehicleService,
  getAllVehiclesService,
  getAllTicketsService,
  getVehicleByIdService,
  resetVehicleService,
  bookTicketService,
  cancelTicketService,
  bookTicketHelperService,
} from "../services/vehicle.service";
import { getTickets } from "../services/user.service";
import { ValidationError } from "../middleware/customErrorClass";
import Logger  from "../loggers/dev-logger";


export const addVehicle = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const vehicle = await addVehicleService(req.body);
    if(vehicle){
      Logger.info(`Message :  "New Vehicle Added" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(201).send(vehicle);
    } else{
      throw new ValidationError("error in adding the vehicle", 403);
    }
  }

export const resetVehicle = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const vehicle = await resetVehicleService(req.params.id);
    return res.status(201).send(vehicle);
  }

export const getAllTickets = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const tickets = await getAllTicketsService();
    return res.status(200).send(tickets);
  }

export const getAllVehicles = async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const vehicles = await getAllVehiclesService();
    Logger.info(`Message :  "Admin query for all vehicles" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
    return res.status(200).send(vehicles);
  }

export const getVehicleById =  async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const id = req.params.id;
    const vehicle = await getVehicleByIdService(id);
    return res.status(200).send(vehicle);
  }


export const bookTicket = async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const check = await bookTicketHelperService(
      req.body.categoryId,
      req.body.spotNumber
    );
    console.log(check);
    if (check) {
      const ticket = await bookTicketService(req.body, req.user._id);
      if (ticket) {
      Logger.info(`Message :  "Ticket Booked by user" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
        return res.status(201).send(ticket);
      }
    } else {
      throw new ValidationError("Seat is not available", 403);
    }
  }

export const cancelTicket = async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const _id = req.params.id;
    const ticket = await cancelTicketService(_id);
    if (ticket) {
      Logger.info(`Message :  "Ticket cancelled by user" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(201).send(ticket);
    } else {
      throw new ValidationError("Ticket not found!", 403);
    }
  }

export const getMyTickets = async (
  req: UserRequest, 
  res: Response
  ): Promise<Response> => {
    const tickets = await getTickets(req.user._id);
    return res.send(tickets);
  }

