import {  Response } from "express";
import { UserRequest } from "../models/user";
import {
  addPlaceService,
  getAllPlacesService,
  getPlaceByIdService,
  bookTicketService,
  cancelTicketService,
  getAllTicketsService,
  bookTicketHelperService,
} from "../services/place.service";
import { getTickets } from "../services/user.service";
import { ValidationError } from "../middleware/customErrorClass";
import Logger from "../loggers/dev-logger";

export const addPlace = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const place = await addPlaceService(req.body);
    if(place){
      Logger.info(`Message :  "Placed add by admin" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(201).send(place);
    } else{
      throw new ValidationError("error in adding the place", 403);
    }
  };

export const getAllTickets = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const tickets = await getAllTicketsService();
    return res.send(tickets);
  }

export const getMyTickets = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const tickets = await getTickets(req.user._id);
    return res.send(tickets);
  }

export const getAllPlaces = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const places = await getAllPlacesService();
    return res.send(places);
  }

export const getPlaceById = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const id = req.params.id;
    const place = await getPlaceByIdService(id);
    return res.send(place);
  }

export const bookTicket = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const check = await bookTicketHelperService(
      req.body.categoryId,
      req.body.spotNumber
    );
    console.log(check);
    if (check) {
      const ticket = await bookTicketService(req.body, req.user._id);
      if (ticket) {
        Logger.info(`Message :  "Ticket booked by user" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
        return res.status(201).send(ticket);
      }
    } else {
      throw new ValidationError("Spot is not available", 403);
    }
  }

export const cancelTicket = async (
    req: UserRequest,
    res: Response,
  ): Promise<Response> => {
    const _id = req.params.id;
    const ticket = await cancelTicketService(_id);
    if (ticket) {
      Logger.info(`Message :  "Ticket Booked by user" - URL : ${req.originalUrl} - Method : ${req.method} - Body : ${JSON.stringify(req.body)} - IP : ${req.ip}`)
      return res.status(201).send(ticket);
    } else {
      throw new ValidationError("Ticket not found!", 403);
    }
  }
