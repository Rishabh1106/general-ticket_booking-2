import express from "express";
import cors from "cors";

import userRouter from "./routers/userRouters";
import travelRouter from "./routers/travelRouters";
import placeRouter from "./routers/placeRouters";
import { errorResponder } from "./middleware/errorMiddleware";
import { errorLoggerWinston } from "./middleware/loggerMiddleware";
import {reqResLogger} from "./loggers/req-res-logger";

require("./db/mongoose");

export const app = express();

app.use(express.json());

const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

//app.use(reqResLogger);

app.use("/api/users", userRouter);
app.use("/api/travel/", travelRouter);
app.use("/api/place", placeRouter);

app.use(errorLoggerWinston);
app.use(errorResponder);
