import Ticket, { TicketInterface } from "../models/ticket";
import Place, { PlaceInterface } from "../models/place";
import { omit } from "lodash";
import { LeanDocument, ObjectId } from "mongoose";
import { ValidationError } from "../middleware/customErrorClass";

export const addPlaceService = async (
  placeInput: PlaceInterface
): Promise<Omit<LeanDocument<PlaceInterface & { _id: string }>, "__v">> => {
  const place = new Place(placeInput);
  place.spots = Array.from({length:place.totalSpots},(_, i) => i + 1);
  await place.save();
  return omit(place.toJSON(), ["__v"]);
};

export const getAllTicketsService = async (): Promise<
  LeanDocument<TicketInterface & { _id: string }>[]
> => {
  const tickets = await Ticket.find({});
  return tickets;
};

export const getAllPlacesService = async (): Promise<
  LeanDocument<PlaceInterface & { _id: string }>[]
> => {
  const places = await Place.find({});
  return places;
};

export const getPlaceByIdService = async (
  _id: string
): Promise<LeanDocument<PlaceInterface & { _id: string }>> => {
  const place = await Place.findOne({ _id: _id });
  if (!place) {
    throw new ValidationError(
      "Place not found please enter valid place Id",
      404
    );
  }
  return place;
};

export const bookTicketHelperService = async (
  _id: string,
  spotNumber: number
): Promise<LeanDocument<boolean>> => {
  const place = await Place.findByIdAndUpdate({ _id: _id });
  if (!place) {
    return false;
  }
  const spotnumber = place.spots.find((e) => {
    return e == spotNumber;
  });
  if (!spotnumber) {
    return false;
  }
  if (place && spotnumber) {
    place.spots = place.spots.filter((s): boolean => {
      return s != spotNumber;
    });
    await place.save();
    return true;
  }
};

export const bookTicketService = async (
  ticketInput: TicketInterface,
  userId: ObjectId
): Promise<LeanDocument<TicketInterface & { _id: string }>> => {
  const ticket = new Ticket(ticketInput);
  ticket.status = "Confirmed";
  ticket.owner = userId;
  await ticket.save();
  return ticket;
};

export const cancelTicketService = async (
  _id: string
): Promise<false | LeanDocument<TicketInterface & { _id: string }>> => {
  const ticket = await Ticket.findOne({ _id: _id });
  if (ticket) {
    ticket.status = "Cancelled";
    const spot = ticket.spotNumber;
    await ticket.save();

    // make seat available again
    const place = await Place.findById(ticket.categoryId);
    place.spots.push(spot);
    await place.save();
    return ticket;
  } else {
    return false;
  }
};
