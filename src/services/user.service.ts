import User, { UserInterface } from "../models/user";
import Ticket, { TicketInterface } from "../models/ticket";
import jwt from "jsonwebtoken";
import * as bcrypt from "bcryptjs";
import { omit } from "lodash";
import { LeanDocument, ObjectId } from "mongoose";

export const signToken = (id: ObjectId): string => {
  return jwt.sign({ id: id }, process.env.jwt_secret_key, {
    expiresIn: process.env.jwt_expires_in,
  });
};

export const validatePassword = async (
  password: string,
  userPassword: UserInterface["password"]
): Promise<boolean> => await bcrypt.compare(password, userPassword);

export const createAdmin = async (
  userInput: UserInterface
): Promise<
  Omit<LeanDocument<UserInterface & { _id: string }>, "__v" | "password">
> => {
  const user = new User(userInput);
  user.role = "admin";

  await user.save();
  return omit(user.toJSON(), ["password", "__v"]);
};

export const createUser = async (
  userInput: UserInterface
): Promise<
  Omit<LeanDocument<UserInterface & { _id: string }>, "__v" | "password">
> => {
  const user = new User(userInput);
  user.role = "user";
  await user.save();
  return omit(user.toJSON(), ["password", "__v"]);
};

export const validateUser = async (
  email: UserInterface["email"],
  password: UserInterface["password"]
): Promise<
  | false
  | Omit<LeanDocument<UserInterface & { _id: string }>, "__v" | "password">
> => {
  const user = await User.findOne({ email });
  if (!user) {
    return false;
  }
  if (await validatePassword(password, user.password)) {
    return omit(user.toJSON(), ["password", "__v"]);
  }
};

export const validateAdminUser = async (
  email: UserInterface["email"],
  password: UserInterface["password"]
): Promise<
  | false
  | Omit<LeanDocument<UserInterface & { _id: string }>, "__v" | "password">
> => {
  const user = await User.findOne({ email });
  if (!user || user.role != "admin") {
    return false;
  }
  if (await validatePassword(password, user.password)) {
    return omit(user.toJSON(), ["password", "__v"]);
  }
};

export const getTickets = async (
  _id: ObjectId
): Promise<LeanDocument<TicketInterface & { _id: string }>[]> => {
  const tickets = await Ticket.find({ owner: _id });
  return tickets;
};
