import Ticket, { TicketInterface } from "../models/ticket";
import Vehicle, { VehicleInterface } from "../models/vehicle";
import { omit } from "lodash";
import { LeanDocument, ObjectId } from "mongoose";
import { ValidationError } from "../middleware/customErrorClass";

export const addVehicleService = async (
  vehicleInput: VehicleInterface
): Promise<Omit<LeanDocument<VehicleInterface & { _id: string }>, "__v">> => {
  const vehicle = new Vehicle(vehicleInput);
  vehicle.seats = Array.from({length:vehicle.totalSeats},(_, i) => i + 1);
  await vehicle.save();
  return omit(vehicle.toJSON(), ["__v"]);
};

export const resetVehicleService = async (
  _id: string
): Promise<Omit<LeanDocument<VehicleInterface & { _id: string }>, "__v">> => {
  const vehicle = await Vehicle.findOne({ _id: _id });
  if(!vehicle){
    throw new ValidationError("vehicle not found", 403);
  }
  vehicle.seats = Array.from({length:vehicle.totalSeats},(_, i) => i + 1);

  await vehicle.save();
  return omit(vehicle.toJSON(), ["__v"]);
};

export const getAllTicketsService = async (): Promise<
  LeanDocument<TicketInterface & { _id: string }>[]
> => {
  const tickets = await Ticket.find({});
  return tickets;
};

export const getAllVehiclesService = async (): Promise<
  LeanDocument<VehicleInterface & { _id: string }>[]
> => {
  const vehicles = await Vehicle.find({});
  return vehicles;
};

export const getVehicleByIdService = async (
  _id: string
): Promise<LeanDocument<VehicleInterface & { _id: string }>> => {
  const vehicle = await Vehicle.findOne({ _id: _id });
  if (!vehicle) {
    throw new ValidationError(
      "Vehicle not found please enter valid Vehicle Id",
      404
    );
  }
  return vehicle;
};

export const bookTicketHelperService = async (
  _id: string,
  spotNumber: number
): Promise<LeanDocument<boolean>> => {
  const vehicle = await Vehicle.findByIdAndUpdate({ _id: _id });
  if (!vehicle) {
    return false;
  }
  const seatnumber = vehicle.seats.find((e) => {
    return e == spotNumber;
  });
  if (!seatnumber) {
    return false;
  }
  if (vehicle && seatnumber) {
    vehicle.seats = vehicle.seats.filter((s): boolean => {
      return s != spotNumber;
    });
    await vehicle.save();
    return true;
  }
};

export const bookTicketService = async (
  ticketInput: TicketInterface,
  userId: ObjectId
): Promise<LeanDocument<TicketInterface & { _id: string }>> => {
  const ticket = new Ticket(ticketInput);
  ticket.status = "Confirmed";
  ticket.owner = userId;
  await ticket.save();
  return ticket;
};

export const cancelTicketService = async (
  _id: string
): Promise<false | LeanDocument<TicketInterface & { _id: string }>> => {
  const ticket = await Ticket.findOne({ _id: _id });
  if (ticket) {
    ticket.status = "Cancelled";
    const seat = ticket.spotNumber;
    await ticket.save();

    // make seat available again
    const vehicle = await Vehicle.findById(ticket.categoryId);
    vehicle.seats.push(seat);
    await vehicle.save();
    return ticket;
  } else {
    return false;
  }
};
