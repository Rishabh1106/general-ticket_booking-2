import { Schema } from "mongoose";
import mongoose from "mongoose";

export interface TicketInterface extends mongoose.Document {
  name: string;
  age: number;
  category: string;
  categoryId: Schema.Types.ObjectId;
  spotNumber: number;
  status: string;
  owner: mongoose.Schema.Types.ObjectId;
}

const ticketSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  spotNumber: {
    type: Number,
    required: true,
  },
  status: {
    type: String,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
});

export default mongoose.model<TicketInterface>("Ticket", ticketSchema);
