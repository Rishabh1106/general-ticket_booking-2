import mongoose from "mongoose";
// Place : hotel/shows etc
// spots : rooms/seats
// type : AC/Non-AC

export interface PlaceInterface extends mongoose.Document {
  name: string;
  location: string;
  peopleInvolved: [string];
  duration: string;
  price: number;
  totalSpots: number;
  spots: number[];
  startTime: Date;
  endTime: Date;
  type: string;
}

const placeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  place: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  peopleInvolved: {
    type: [String],
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  totalSpots: {
    type: Number,
    required: true,
  },
  spots: {
    type: [Number],
  },
  price: {
    type: Number,
    required: true,
  },
  startTime: {
    type: Date,
    required: true,
  },
  endTime: {
    type: Date,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

export default mongoose.model<PlaceInterface>("Place", placeSchema);
