import mongoose from "mongoose";

export interface VehicleInterface extends mongoose.Document {
  vehicleNumber: string;
  vehicleName: string;
  source: string;
  destination: string;
  totalSeats: number;
  seats: number[];
  fare: number;
  startDate: Date;
  endDate: Date;
}

const vehicleSchema = new mongoose.Schema({
  vehicleNumber: {
    type: String,
    required: true,
    unique: true,
  },
  vehicleName: {
    type: String,
    required: true,
  },
  vehicle: {
    type: String,
    required: true,
  },
  source: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  totalSeats: {
    type: Number,
    required: true,
  },
  seats: {
    type: [Number],
  },
  fare: {
    type: Number,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
});

export default mongoose.model<VehicleInterface>("Vehicle", vehicleSchema);
