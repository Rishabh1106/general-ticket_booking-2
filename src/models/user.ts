import mongoose from 'mongoose';
import * as validator from 'validator';
import * as bcrypt from 'bcryptjs';
import {Request} from 'express';



export interface UserInterface extends mongoose.Document {
  name : string,
  email : string,
  password : string,
  age : number,
  role : string
}

export interface UserRequest extends Request{
  user?: UserInterface;
}

const userSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        trim : true
    },
    email : {
        type : String,
        required : true,
        trim : true,
        unique : true,
        lowercase : true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error("Email is invalid");
            }
        }
    },
    password : {
        type : String,
        required : true,
        minlength : 7,
        trim : true,
        validate(value){
            if(value.toLowerCase().includes("password")){
                throw new Error("Password can not contain 'password'");
            }
        },
    },
    age: {
        type: Number,
        default: 0,
        validate(value) {
          if (value < 0) {
            throw new Error("Age must be a postive number");
          }
        },
      },
      role: {
        type: String,
      },
      
})


// giving reference to tickets schema
userSchema.virtual('tickets',{
    ref : 'Ticket',
    localField : '_id',
    foreignField : 'owner'
})

userSchema.pre("save",async function (this: UserInterface,next) {
    //const user = this;
  
    if (this.isModified("password")) {
      this.password = await bcrypt.hash(this.password, 8);
    }
  
    next();
  })

  
    
  export default mongoose.model<UserInterface>("User", userSchema);
  


