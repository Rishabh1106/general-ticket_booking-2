import express from 'express';
import {adminAccess, auth} from '../middleware/auth';
import * as placeController from '../controllers/placeControllers';
import { wrapperFunction } from './wrapperFunction';

const router = express.Router();

router.post('/addPlace', auth, adminAccess, wrapperFunction(placeController.addPlace)); // to add the place
router.get('/getAllTickets', auth, adminAccess, wrapperFunction(placeController.getAllTickets)); // get all the tickets 

router.get('/getAllPlaces', auth, wrapperFunction(placeController.getAllPlaces)); // get all places and their details
router.get('/getPlace/:id', auth, wrapperFunction(placeController.getPlaceById)); // get place deatils and availability
router.post('/bookTicket', auth, wrapperFunction(placeController.bookTicket)); // make a booking
router.patch('/cancelTicket/:id', auth, wrapperFunction(placeController.cancelTicket)); // cancel ticket
router.get('/getMyTickets', auth, wrapperFunction(placeController.getMyTickets)); // get the tickets booked by me


export = router;
