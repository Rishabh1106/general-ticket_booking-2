import express from "express";
import { auth } from "../middleware/auth";

import * as userController from "../controllers/userControllers";
import { wrapperFunction } from "./wrapperFunction";

const router = express.Router();

router.post("/registerAdmin", wrapperFunction(userController.registerAdmin)); // to register admin
router.post("/loginAdmin", wrapperFunction(userController.loginAdmin)); // to login admin
router.post("/registerUser", wrapperFunction(userController.registerUser)); // to register user
router.post("/loginUser", wrapperFunction(userController.loginUser)); // to login user
router.get("/me", auth, wrapperFunction(userController.usersMe)); // to get user details

export = router;