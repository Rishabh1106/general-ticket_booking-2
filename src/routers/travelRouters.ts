import express from "express";
import { adminAccess, auth } from "../middleware/auth";
import * as vehicleController from "../controllers/vehicleControllers";
import { wrapperFunction } from "./wrapperFunction";

const router = express.Router();

router.post("/addVehicle", auth, adminAccess, wrapperFunction(vehicleController.addVehicle)); // to add the bus
router.post("/resetVehicle/:id", auth, adminAccess, wrapperFunction(vehicleController.resetVehicle)); // reset the bus to make all seats available again
router.get("/getAllTickets", auth, adminAccess, wrapperFunction(vehicleController.getAllTickets)); // get all the tickets

router.get("/getAllVehicles", auth, wrapperFunction(vehicleController.getAllVehicles)); // get all buses and their details
router.get("/getVehicle/:id", auth, wrapperFunction(vehicleController.getVehicleById)); // get bus deatils and available seats
router.post("/bookTicket", auth, wrapperFunction(vehicleController.bookTicket)); // book a ticket
router.patch("/cancelTicket/:id", auth, wrapperFunction(vehicleController.cancelTicket)); // cancel ticket
router.get("/getMyTickets", auth, wrapperFunction(vehicleController.getMyTickets)); // get the tickets booked by me

export = router;
