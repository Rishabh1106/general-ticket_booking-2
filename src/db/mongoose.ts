import mongoose from "mongoose";

const db: string = process.env.DB;

async function mongoConnection() {
  try {
    await mongoose.connect(db);
    console.log("Connected to mongodb...");
  } catch (err) {
    console.log("Err in connecting to mongo ", err);
  }
}

mongoConnection();
